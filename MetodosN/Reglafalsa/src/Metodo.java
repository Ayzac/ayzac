import java.sql.SQLOutput;
import java.util.Scanner;

public class Metodo {
    static double f(double x) {
        return (6*Math.pow(x,5))-(2*(Math.pow(x,4))+(2));
    }

    public static void main(String[] args) {

        double a, b, m = 0;
        int iteracion = 0;
        double error = 1;
        double mold = 0;
        double porcError;
        int x;
        String uno="";
        String dos="";
        String tres="";


        Scanner leer = new Scanner(System.in);
        Arreglos e = new Arreglos();

        System.out.println("ingrese los elementos que tendra la ecuacion");
        x=leer.nextInt();
        e.IniciarArreglos(x);


        for (int i = 0; i < e.arre2.length; i++)
        {
            System.out.println("Constante o variable?\nEscribe C o V");
            String primeraFase = leer.next();

            if (primeraFase.equals("C") || primeraFase.equals("c"))
            {
                System.out.println("La constante es positiva o negativa?\nEscribe P o N");
                String constanteFaseUno = leer.next();
                boolean flagUno = true;
                boolean operacion = false;
                do
                {
                    if (constanteFaseUno.equals("P") || constanteFaseUno.equals("p"))
                    {
                        operacion = true;
                    }
                    else if (constanteFaseUno.equals("N") || constanteFaseUno.equals("n"))
                    {
                        operacion = false;
                    }
                    else
                    {
                        System.out.println("No puedes colocar un digito o caracter distinto");
                        constanteFaseUno = "";
                        flagUno = false;
                    }
                } while (flagUno != true);

                System.out.println("Ingrese el numero, solo valores positivos:");
                String constanteFaseDos = leer.next();
                e.arre2[i] = Double.parseDouble(constanteFaseDos);

                if (operacion)
                {
                    e.arre1[i] = 1;
                }
                else
                {
                    String aux2 = "-";

                    String aux = aux2 + 1;

                    e.arre1[i] = Double.parseDouble(aux);
                }
                e.arre3[i] = 1;

            }
            else if (primeraFase.equals("V") || primeraFase.equals("v"))
            {
                System.out.println("Tiene alguna constante para multiplicar por la variable?");
                double variableFaseDos =  Double.parseDouble(leer.next());

                System.out.println("La Variable es positiva o negativa?\nEscribe P o N");
                String variableFaseUno = leer.next();
                boolean flagUno = true;
                boolean operacion = false;
                do
                {
                    if (variableFaseUno.equals("P") || variableFaseUno.equals("p"))
                    {
                        operacion = true;
                    }
                    else if (variableFaseUno.equals("N") || variableFaseUno.equals("n"))
                    {
                        operacion = false;
                    }
                    else
                    {
                        System.out.println("No puedes colocar un digito o caracter distinto");
                        variableFaseUno = "";
                        flagUno = false;
                    }
                } while (flagUno != true);

                System.out.println("Ingrese el exponente de la variable:");
                double variableFaseTres = Double.parseDouble(leer.next());
                e.arre3[i] = variableFaseTres;

                if (operacion)
                {
                    e.arre1[i] = variableFaseDos;
                }
                else
                {
                    String aux2 = "-";

                    String aux = aux2 + variableFaseDos;

                    variableFaseDos = Double.parseDouble(aux);

                    e.arre1[i] = variableFaseDos;
                }

                e.arre2[i] = 100;

            }
            else
            {
                System.out.println("No puedes colocar un digito o caracter distinto");
                primeraFase = "";
                i--;
            }
        }
        //hasta este punto ya tenemos la funcion
        //lo siguiente es mostrarla en pantalla

        for (int i = 0; i < e.arre2.length; i++)
        {
            if (e.arre2[i] == 100)
            {
                if (e.arre1[i] > 1 || e.arre1[i] == 1)
                {
                    if (i == 0 || i == e.arre2.length)
                    {
                        if (e.arre1[i] == 1)
                        {
                            uno = "X";
                        }
                        else
                        {
                            uno = e.arre1[i] + "X";
                        }
                    }
                    else
                    {
                        if (e.arre1[i] == 1)
                        {
                            uno = "+X";
                        }
                        else
                        {
                            uno = "+" + e.arre1[i] + "X";
                        }
                    }
                }
                else if (e.arre1[i] < 0)
                {
                    if (e.arre1[i] == -1)
                    {
                        uno = "-X";
                    }
                    else
                    {
                        uno = e.arre1[i] + "X";
                    }
                }

                if (e.arre3[i] < 2)
                {
                    dos = uno;
                }
                else
                {
                    dos = uno + "^" + e.arre3[i];
                }

            }
            else
            {
                if (e.arre1[i] > 1 || e.arre1[i] == 1)
                {
                    if (i == 0 || i == e.arre2.length)
                    {
                        uno = (e.arre1[i] * e.arre2[i]) + "";
                    }
                    else
                    {
                        uno = "+" + (e.arre1[i] * e.arre2[i]);
                    }
                }
                else if (e.arre1[i] < 0)
                {
                    uno = (e.arre1[i] * e.arre2[i]) + "";
                }
                dos = uno;
            }
            tres = tres + dos;
        }
        System.out.println(tres);
//___________________________________
        System.out.println("Ingrese el valor de A: ");
        a = leer.nextDouble();
        System.out.println("Ingrese el valor de B: ");
        b = leer.nextDouble();
        System.out.println("Ingrese el porcentaje de error: ");
        porcError = leer.nextFloat();

        boolean flag = true;
        double errorRelativo = 0;
        double errorNuevo = 0;
        double errorViejo = 0;
        double Xr = 0;
        double A = (a);
        double B = (b);
        int z = 0;
        do
        {
            if (z == 0)
            {
                errorNuevo = Xr;
                errorViejo = A;
            }
            else
            {
                errorViejo = errorNuevo;
                errorNuevo = Xr;

            }

            if (!(z == 0))
                errorRelativo = Math.abs((errorNuevo - errorViejo) / errorNuevo) * 100;


            if (errorRelativo < porcError && z !=0)
            {
                System.out.println("La raiz es : " + Xr);
            }
            else
            {
                double fX0 = f(A);
                double fX1 = f(B);
                Xr = ((fX0 * A) - (fX1 * B)) / (fX0 - fX1);

                if (fX0 * fX1 < 0)
                {
                    A = Xr;
                }
                else
                {
                    B = Xr;
                }
                z++;
                System.out.println("H"+ z);
            }
        } while (flag != true);

    }
}