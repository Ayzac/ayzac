import java.util.Scanner;

public class Trigonometrica{

    static double f(double x) { return (((0.5)*(Math.pow(x, 3))) + ((3.82)*(x)) - (27)); }

    //seno
    static double Fs(double f){return Math.sin(f);}
    //coseno
    static double Fc(double f){return Math.cos(f);}
    //tangente
    static double Ft(double f){return Math.tan(f);}
    //secante
    static double Fsc(double f){return 1/(Math.cos(f));}
    //cosecante
    static double Fcs(double f){return 1/(Math.sin(f));}
    //cotangente
    static double Fct(double f){return 1/(Math.tan(f));}

    public static void main(String[] args) {

        Scanner teclado = new Scanner(System.in);
        double a, b, e, eR, a1, b1, ax, bx;
        double xR, xRN;
        int op, op2, contador = 0;

        System.out.println("Valores en: Radianes -> 1     Grados -> 2");
        op = teclado.nextInt();
        System.out.println(("Operacion que acompaña f(x):\n1. Seno\n2. Coseno\n3. Tangente\n4. Secante\n5. Cosecante\n6. Cotangente "));
        op2 = teclado.nextInt();
        switch(op) {
            case 1:
                switch (op2) {
                    case 1:
                        System.out.println("Ingresa el valor de a  : ");
                        a = teclado.nextDouble();
                        System.out.println("Ingresa el valor de b: ");
                        b = teclado.nextDouble();
                        System.out.println("Ingresa el porcentaje de error: ");
                        e = teclado.nextDouble();

                        a1=Fs(f(a));
                        b1=Fs(f(b));
                        System.out.println("A: "+a1+" B: "+b1);
                        xR= (a1*b - b1*a)/ (a1-b1);
                        System.out.printf("XR: %f ",xR);
                        System.out.printf("b1: %f ",b1);

                        if(Fs(f(a))*Fs(f(xR))==0) {
                            System.out.printf("La raiz es: %f", b);
                        } else if(Fs(f(a))*Fs(f(xR))<0) {
                            b1=xR;
                        } else {
                            do {
                                xRN = b - ((b1 * xR - b1 * b) / (Fs(f(xR)) - b1));
                                eR = Math.abs((xRN - xR) / (xRN)) * 100;
                                System.out.printf("\nXRN: %f   Error Relativo: %f", xRN, eR);
                                xR = xRN;
                            } while (eR >= e);
                        }
                        break;
                    case 2:
                        System.out.println("Ingresa el valor de a  : ");
                        a = teclado.nextDouble();
                        System.out.println("Ingresa el valor de b: ");
                        b = teclado.nextDouble();
                        System.out.println("Ingresa el porcentaje de error: ");
                        e = teclado.nextDouble();

                        a1=Fc(f(a));
                        b1=Fc(f(b));
                        System.out.println("A: "+a1+" B: "+b1);
                        xR= (a1*b - b1*a)/ (a1-b1);
                        System.out.printf("XR: %f ",xR);
                        System.out.printf("b1: %f ",b1);

                        if(Fc(f(a))*Fc(f(xR))==0) {
                            System.out.printf("La raiz es: %f", b);
                        } else if(Fc(f(a))*Fc(f(xR))<0) {
                            b1=xR;
                        } else {
                            do {

                                xRN = b - ((b1 * xR - b1 * b) / (Fc(f(xR)) - b1));
                                eR = Math.abs((xRN - xR) / (xRN)) * 100;
                                System.out.printf("\nXRN: %f   Error Relativo: %f", xRN, eR);
                                xR = xRN;
                            } while (eR >= e);
                        }
                        break;
                    case 3:
                        System.out.println("Ingresa el valor de a  : ");
                        a = teclado.nextDouble();
                        System.out.println("Ingresa el valor de b: ");
                        b = teclado.nextDouble();
                        System.out.println("Ingresa el porcentaje de error: ");
                        e = teclado.nextDouble();

                        a1=Ft(f(a));
                        b1=Ft(f(b));
                        System.out.println("A: "+a1+" B: "+b1);
                        xR= (a1*b - b1*a)/ (a1-b1);
                        System.out.printf("XR: %f ",xR);
                        System.out.printf("b1: %f ",b1);

                        if(Ft(f(a))*Ft(f(xR))==0) {
                            System.out.printf("La raiz es: %f", b);
                        } else if(Ft(f(a))*Ft(f(xR))<0) {
                            b1=xR;
                        } else {
                            do {

                                xRN = b - ((b1 * xR - b1 * b) / (Ft(f(xR)) - b1));
                                eR = Math.abs((xRN - xR) / (xRN)) * 100;
                                System.out.printf("\nXRN: %f   Error Relativo: %f", xRN, eR);
                                xR = xRN;
                            } while (eR >= e);
                        }
                        break;
                    case 4:
                        System.out.println("Ingresa el valor de a  : ");
                        a = teclado.nextDouble();
                        System.out.println("Ingresa el valor de b: ");
                        b = teclado.nextDouble();
                        System.out.println("Ingresa el porcentaje de error: ");
                        e = teclado.nextDouble();

                        a1=Fsc(f(a));
                        b1=Fsc(f(b));
                        xR= (a1*b - b1*a)/ (a1-b1);
                        System.out.printf("XR: %f ",xR);
                        System.out.printf("b1: %f ",b1);

                        if(Fsc(f(a))*Fsc(f(xR))==0) {
                            System.out.printf("La raiz es: %f", b);
                        } else if(Fsc(f(a))*Fsc(f(xR))<0) {
                            b1=xR;
                        } else {
                            do {
                                xRN = b - ((b1 * xR - b1 * b) / (Fsc(f(xR)) - b1));
                                eR = Math.abs((xRN - xR) / (xRN)) * 100;
                                System.out.printf("\nXRN: %f   Error Relativo: %f", xRN, eR);
                                xR = xRN;

                            } while (eR >= e);
                        }
                        break;
                    case 5:
                        System.out.println("Ingresa el valor de a  : ");
                        a = teclado.nextDouble();
                        System.out.println("Ingresa el valor de b: ");
                        b = teclado.nextDouble();
                        System.out.println("Ingresa el porcentaje de error: ");
                        e = teclado.nextDouble();

                        a1=Fcs(f(a));
                        b1=Fcs(f(b));
                        xR= (a1*b - b1*a)/ (a1-b1);
                        System.out.printf("XR: %f ",xR);
                        System.out.printf("b1: %f ",b1);

                        if(Fcs(f(a))*Fcs(f(xR))==0) {
                            System.out.printf("La raiz es: %f", b);
                        } else if(Fcs(f(a))*Fcs(f(xR))<0) {
                            b1=xR;
                        } else {
                            do {
                                xRN = b - ((b1 * xR - b1 * b) / (Fcs(f(xR)) - b1));
                                eR = Math.abs((xRN - xR) / (xRN)) * 100;
                                System.out.printf("\nXRN: %f   Error Relativo: %f", xRN, eR);
                                xR = xRN;
                            } while (eR >= e);
                        }
                        break;
                    case 6:
                        System.out.println("Ingresa el valor de a  : ");
                        a = teclado.nextDouble();
                        System.out.println("Ingresa el valor de b: ");
                        b = teclado.nextDouble();
                        System.out.println("Ingresa el porcentaje de error: ");
                        e = teclado.nextDouble();

                        a1=Fct(f(a));
                        b1=Fct(f(b));
                        xR= (a1*b - b1*a)/ (a1-b1);
                        System.out.printf("XR: %f ",xR);
                        System.out.printf("b1: %f ",b1);
                        if(Fct(f(a))*Fct(f(xR))==0) {
                            System.out.printf("La raiz es: %f", b);
                        } else if(Fct(f(a))*Fct(f(xR))<0) {
                            b1=xR;
                        } else {
                            do {
                                xRN = b - ((b1 * xR - b1 * b) / (Fct(f(xR)) - b1));
                                eR = Math.abs((xRN - xR) / (xRN)) * 100;
                                System.out.printf("\nXRN: %f   Error Relativo: %f", xRN, eR);
                                xR = xRN;
                            } while (eR >= e);
                        }
                        break;
                    default:
                        System.out.println("Valores invalidos.");
                        break;
                }
                break;
            case 2:
                switch (op2) {
                    case 1:
                        System.out.println("Ingresa el valor de a  : ");
                        a = teclado.nextDouble();
                        System.out.println("Ingresa el valor de b: ");
                        b = teclado.nextDouble();
                        System.out.println("Ingresa el porcentaje de error: ");
                        e = teclado.nextDouble();

                        ax = Math.toRadians(a);
                        bx = Math.toRadians(b);

                        a1=Fs(f(Math.toRadians(a)));
                        b1=Fs(f(Math.toRadians(b)));
                        System.out.println("A: "+a1+" B: "+b1);

                        xR= (a1*bx - b1*ax)/ (a1-b1);
                        System.out.printf("XR: %f ",xR);
                        System.out.printf("b1: %f ",b1);

                        if(Fs(f(a))*Fs(f(xR))==0) {
                            System.out.printf("La raiz es: %f", b);
                        } else if(Fs(f(a))*Fs(f(xR))<0) {
                            b1=xR;
                        } else {
                            do {
                                xRN = bx - ((b1 * xR - b1 * bx) / (Fs(f(xR)) - b1));
                                eR = Math.abs((xRN - xR) / (xRN)) * 100;
                                System.out.printf("\nXRN: %f   Error Relativo: %f", xRN, eR);
                                xR = xRN;
                            } while (eR >= e);
                        }
                        break;
                    case 2:
                        System.out.println("Ingresa el valor de a  : ");
                        a = teclado.nextDouble();
                        System.out.println("Ingresa el valor de b: ");
                        b = teclado.nextDouble();
                        System.out.println("Ingresa el porcentaje de error: ");
                        e = teclado.nextDouble();

                        ax = Math.toRadians(a);
                        bx = Math.toRadians(b);
                        a1=Fc(f(Math.toRadians(a)));
                        b1=Fc(f(Math.toRadians(b)));
                        System.out.println("A: "+a1+" B: "+b1);
                        xR= (a1*bx - b1*ax)/ (a1-b1);
                        System.out.printf("XR: %f ",xR);
                        System.out.printf("b1: %f ",b1);

                        if(Fc(f(a))*Fc(f(xR))==0) {
                            System.out.printf("La raiz es: %f", b);
                        } else if(Fc(f(a))*Fc(f(xR))<0) {
                            b1=xR;
                        } else {
                            do {
                                xRN = bx - ((b1 * xR - b1 * bx) / (Fc(f(xR)) - b1));
                                eR = Math.abs((xRN - xR) / (xRN)) * 100;
                                System.out.printf("\nXRN: %f   Error Relativo: %f", xRN, eR);
                                xR = xRN;
                            } while (eR >= e);
                            break;
                        }
                    case 3:
                        System.out.println("Ingresa el valor de a  : ");
                        a = teclado.nextDouble();
                        System.out.println("Ingresa el valor de b: ");
                        b = teclado.nextDouble();
                        System.out.println("Ingresa el porcentaje de error: ");
                        e = teclado.nextDouble();

                        ax = Math.toRadians(a);
                        bx = Math.toRadians(b);
                        a1=Ft(f(ax));
                        b1=Ft(f(bx));
                        System.out.println("A: "+a1+" B: "+b1);
                        xR= (a1*bx - b1*ax)/ (a1-b1);
                        System.out.printf("XR: %f ",xR);
                        System.out.printf("b1: %f ",b1);

                        if(Ft(f(a))*Ft(f(xR))==0) {
                            System.out.printf("La raiz es: %f", b);
                        } else if(Ft(f(a))*Ft(f(xR))<0) {
                            b1=xR;
                        } else {
                            do {
                                xRN = bx - ((b1 * xR - b1 * bx) / (Ft(f(xR)) - b1));
                                eR = Math.abs((xRN - xR) / (xRN)) * 100;
                                System.out.printf("\nXRN: %f   Error Relativo: %f", xRN, eR);
                                xR = xRN;
                            } while (eR >= e);
                        }
                        break;
                    case 4:
                        System.out.println("Ingresa el valor de a  : ");
                        a = teclado.nextDouble();
                        System.out.println("Ingresa el valor de b: ");
                        b = teclado.nextDouble();
                        System.out.println("Ingresa el porcentaje de error: ");
                        e = teclado.nextDouble();

                        ax = Math.toRadians(a);
                        bx = Math.toRadians(b);
                        a1=Fsc(f(Math.toRadians(a)));
                        b1=Fsc(f(Math.toRadians(b)));
                        System.out.println("A: "+a1+" B: "+b1);
                        xR= (a1*bx - b1*ax)/ (a1-b1);
                        System.out.printf("XR: %f ",xR);
                        System.out.printf("b1: %f ",b1);

                        if(Fsc(f(a))*Fsc(f(xR))==0) {
                            System.out.printf("La raiz es: %f", b);
                        } else if(Fsc(f(a))*Fsc(f(xR))<0) {
                            b1=xR;
                        } else {
                            do {
                                xRN = bx - ((b1 * xR - b1 * bx) / (Fsc(f(xR)) - b1));
                                eR = Math.abs((xRN - xR) / (xRN)) * 100;
                                System.out.printf("\nXRN: %f   Error Relativo: %f", xRN, eR);
                                xR = xRN;
                            } while (eR >= e);
                        }
                        break;
                    case 5:
                        System.out.println("Ingresa el valor de a  : ");
                        a = teclado.nextDouble();
                        System.out.println("Ingresa el valor de b: ");
                        b = teclado.nextDouble();
                        System.out.println("Ingresa el porcentaje de error: ");
                        e = teclado.nextDouble();

                        ax = Math.toRadians(a);
                        bx = Math.toRadians(b);
                        a1=Fcs(f(Math.toRadians(a)));
                        b1=Fcs(f(Math.toRadians(b)));
                        System.out.println("A: "+a1+" B: "+b1);
                        xR= (a1*bx - b1*ax)/ (a1-b1);
                        System.out.printf("XR: %f ",xR);
                        System.out.printf("b1: %f ",b1);

                        if(Fcs(f(a))*Fcs(f(xR))==0) {
                            System.out.printf("La raiz es: %f", b);
                        } else if(Fcs(f(a))*Fcs(f(xR))<0) {
                            b1=xR;
                        } else {
                            do {
                                xRN = bx - ((b1 * xR - b1 * bx) / (Fcs(f(xR)) - b1));
                                eR = Math.abs((xRN - xR) / (xRN)) * 100;
                                System.out.printf("\nXRN: %f   Error Relativo: %f", xRN, eR);
                                xR = xRN;
                            } while (eR >= e);
                        }
                        break;
                    case 6:
                        System.out.println("Ingresa el valor de a  : ");
                        a = teclado.nextDouble();
                        System.out.println("Ingresa el valor de b: ");
                        b = teclado.nextDouble();
                        System.out.println("Ingresa el porcentaje de error: ");
                        e = teclado.nextDouble();

                        ax = Math.toRadians(a);
                        bx = Math.toRadians(b);

                        a1=Fct(f(Math.toRadians(a)));
                        b1=Fct(f(Math.toRadians(b)));
                        System.out.println("A: "+a1+" B: "+b1);
                        xR= (a1*bx - b1*ax)/ (a1-b1);
                        System.out.printf("XR: %f ",xR);
                        System.out.printf("b1: %f ",b1);

                        if(Fct(f(a))*Fct(f(xR))==0) {
                            System.out.printf("La raiz es: %f", b);
                        } else if(Fct(f(a))*Fct(f(xR))<0) {
                            b1=xR;
                        } else {
                            do {
                                xRN = bx - ((b1 * xR - b1 * bx) / (Fct(f(xR)) - b1));
                                eR = Math.abs((xRN - xR) / (xRN)) * 100;
                                System.out.printf("\nXRN: %f   Error Relativo: %f", xRN, eR);
                                xR = xRN;
                            } while (eR >= e);
                        }
                        break;
                    default:
                        System.out.println("Datos invalidos.");
                        break;
                }
                break;
            default:
                System.out.println("Datos invalidos.");
                break;
        }
    }
}
