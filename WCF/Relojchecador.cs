using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Relojchecador
{
    public partial class Form1 : Form
    {
        DateTime datoEntrada = new DateTime();
        DateTime E = new DateTime();
        DateTime datoSalida = new DateTime();
       
        string r;

       
        bool b = false;
        bool b1 = false;
        int flag = 0;

        public string Path = @"C:\Users\Pepe\Desktop\programas\Examen.txt";
        


        public Form1()
        {
            InitializeComponent();
        }
       

        private void btnIngresar_Click(object sender, EventArgs e)
        {
           r  = comboBox1.Text;

            if (!File.Exists(Path))
            {
                using (StreamWriter sw = File.CreateText(Path))
                {
                    sw.WriteLine(r);
                    sw.WriteLine(datoEntrada);
                    
                }
            }
            else
            {
                string append="";
                if(flag == 1)
                {
                     append = "Entrada\n"+ r + "\n" + datoEntrada + Environment.NewLine;
                }else if(flag ==2)
                {
                     append = "Salida\n" + r + "\n" + datoSalida + Environment.NewLine;
                }
               
                File.AppendAllText(Path, append);
                datoEntrada = E;
                
            }



        }

        private void btnRevisar_Click(object sender, EventArgs e)
        {

            string readText = File.ReadAllText(Path);
            MessageBox.Show(readText);


            //MessageBox.Show("El usuario es : " + r + "\nLa fecha de entrada es : " + datoEntrada+"\n");
        }

        private void rtbnEntrada_CheckedChanged(object sender, EventArgs e)
        {
            btnIngresar.Enabled = true;
            btnRevisar.Enabled = true;
            b = true;
            flag = 1;
        }

        private void rbtnSalida_CheckedChanged(object sender, EventArgs e)
        {
            datoSalida = dateTimePicker1.Value;
            btnEntrar.Enabled = true;
            btnVerificar.Enabled = true;
            b1 = true;
            flag = 2;
        }

        private void dateTimePicker1_ValueChanged(object sender, EventArgs e)
        {
            datoEntrada = dateTimePicker1.Value;
        }
    }
}