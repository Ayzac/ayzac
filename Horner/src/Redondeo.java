public class Redondeo {

    public double redondear(double b, int decimales)
    {

        String h = String.valueOf(b);
        if (h.contains("."))
        {
            String m = h.substring((h.indexOf(".") + 1), h.length());
            if (m.length() > decimales)
            {
                m = m.substring(0, decimales + 1);
                String c = m.substring((m.length() - 1), m.length());
                double j = Double.parseDouble(c);
                if (j >= 5)
                {
                    int x = Integer.parseInt(m);
                    x += (Math.abs(j - 10));
                    m = String.valueOf(x);
                    h = h.substring(0, h.indexOf(".") + 1);
                    h = h + m;
                    b = Double.parseDouble(h);

                }
                else
                {
                    h = h.substring(0, h.indexOf(".") + 1);
                    h = h + m;
                    h = h.substring(0, h.length()-1);
                    b = Double.parseDouble(h);
                }

            }
        }
        return b;
    }

}