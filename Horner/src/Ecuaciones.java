public class Ecuaciones {

    public double[] Funcion;
    public double[] Potencia;
    public double[] Producto;
    public double[] DivisionSinteticaUno;
    public double[] ResiduoUno;
    public double[] ResiduoDos;
    public double iA = 0;
    public double iB = 0;
    String uno = "", dos = "", tres = "";

    public void Iniciar(int a){

        Funcion = new double[a];
        Potencia = new double[a];
        Producto = new double[a];

    }

    public String mostrarFuncion()
    {
        for (int i = 0; i < Funcion.length; i++) {
            if (Funcion[i] == 100) {
                if (Producto[i] > 1 || Producto[i] == 1) {
                    if (i == 0 || i == Funcion.length) {
                        if (Producto[i] == 1) {
                            uno = "X";
                        } else {
                            uno = Producto[i] + "X";
                        }
                    } else {
                        if (Producto[i] == 1) {
                            uno = "+X";
                        } else {
                            uno = "+" + Producto[i] + "X";
                        }
                    }
                } else if (Producto[i] < 0) {
                    if (Producto[i] == -1) {
                        uno = "-X";
                    } else {
                        uno = Producto[i] + "X";
                    }
                } else if (Producto[i] < 1 && Producto[i] > 0) {
                    if (i == 0) {
                        uno = Producto[i] + "X";
                    } else {
                        uno = "+" + Producto[i] + "X";
                    }
                }

                if (Potencia[i] < 2) {
                    dos = uno;
                } else {
                    dos = uno + "^" + Potencia[i];
                }

            } else {
                if (Producto[i] > 1 || Producto[i] == 1) {
                    if (i == 0 || i == Funcion.length) {
                        uno = (Producto[i] * Funcion[i]) + "";
                    } else {
                        uno = "+" + (Producto[i] * Funcion[i]);
                    }


                } else if (Producto[i] < 0) {
                    uno = (Producto[i] * Funcion[i]) + "";
                }


                dos = uno;
            }


            tres = tres + dos;


        }

        return tres;
    }
    // aqui empieza todo lo de Horner
    //con esta funcion lo que hace es que saca el mayor exponente de la funcion introducida
    public int Exponente()
    {
        int c = 0;
        for(int i = 0 ; i < Potencia.length ; i++) {
            if (Potencia[i] > c) {
                c = (int) Potencia[i];
            }
        }
        return c;
    }

    //Con esta funcion lo que hacemos es practicamente  Calcular el residuo de cada iteracion del metodo
    public double HornerR(double pvalor)
    {
        double temp = 0;
        for(int i = 0; i < DivisionSinteticaUno.length ; i++)
        {
            if(i==0)
            {
                temp = DivisionSinteticaUno[i];
            }else
            {
                temp = pvalor * temp;
                temp = DivisionSinteticaUno[i] + temp;
            }
            ResiduoUno[i] = temp;
        }
        return ResiduoUno[ResiduoUno.length-1];
    }

    //con eso hacemos la funcion polinomica a evaluar con divisiones polinomicas
    public void funcionPolinomica()
    {
        double pvalor = Exponente();
        DivisionSinteticaUno = new double[(int)pvalor+1];
        ResiduoUno = new double[(int)pvalor+1];

        for(int i = 0 ; i < DivisionSinteticaUno.length; i++)
        {
            if(!(i==DivisionSinteticaUno.length-1))
            {
                if(exist(pvalor))
                {
                    DivisionSinteticaUno[i] = Producto[indexExist(pvalor)];
                }
                else
                {
                    DivisionSinteticaUno[i] = 0;
                }
            }
            else
            {
                DivisionSinteticaUno[i] = Producto[Producto.length-1];
            }
            pvalor--;
        }
    }
    //esto nos devuelve si el exponente se encuentra en el arreglo de potencia
    public boolean exist(double pvalor)
    {
        for(int i = 0; i < Potencia.length; i++)
        {
            if(pvalor==Potencia[i] && Funcion[i]==100)
            {
                return true;
            }
        }
        return false;
    }
    //esto nos da el index del valor de la potencia que se encontro
    public  int indexExist(double pvalor)
    {
        int temp = 0;
        for(int i = 0; i < Potencia.length; i++)
        {
            if(Potencia[i]==pvalor)
            {
                temp = i;
                break;
            }
        }
        return  temp;
    }

    public double HornerAsterisco(double pvalor)
    {
        ResiduoDos = new double[ResiduoUno.length-1];
        double temp = 0;
        for(int i = 0; i < ResiduoUno.length-1 ; i++)
        {
            if(i==0)
            {
                temp = ResiduoUno[i];
            }else
            {
                temp = pvalor * temp;
                temp = ResiduoUno[i] + temp;
            }
            ResiduoDos[i] = temp;
        }
        return ResiduoDos[ResiduoDos.length-1];
    }

    public double redondeo(double valor, int decimales)
    {
        Redondeo r = new Redondeo();
        return r.redondear(valor,decimales);
    }

    public boolean intervaloUsuario(double a, double b)
    {
        if(HornerR(a) * HornerR(b) <= 0)
        {
            return true;
        }
        return false;
    }

}