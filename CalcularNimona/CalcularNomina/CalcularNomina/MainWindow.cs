﻿using System;
using Gtk;

public partial class MainWindow: Gtk.Window
{
    public MainWindow() : base(Gtk.WindowType.Toplevel)
    {
        Build();
    }

    double sueldobasico, bonificacion=150, dias, sueldoneto;


    protected void OnDeleteEvent(object sender, DeleteEventArgs a)
    {
        Application.Quit();
        a.RetVal = true;

    }
    private void Aguinaldo()
    {
        sueldobasico = float.Parse(txtS.Text);
        dias = float.Parse(txtDN.Text);
        bonificacion = float.Parse(txtB.Text);

        sueldoneto = (sueldobasico * dias) * (bonificacion);

        lblMostrar.Text = "$" + Convert.ToString(Math.Round(sueldoneto, 2));
    }
   
    private void IMSS()
    {
        double sueldobase= Double.Parse(txtS.Text);
        double nomina = Double.Parse(txtDN.Text);
        double auxA, auxB, auxC;
      

        auxA = sueldobase * 0.0025;
        auxB = sueldobase * 0.00375;
        auxC = sueldobase * 0.00625;

        sueldobase = (auxA + auxB + auxC);

        //Console.Write(sueldo);
        lblIMSS.Text = "$" + Convert.ToString(Math.Round(sueldobase, 2));
    }
    private void RCV()
    {
        double sueldo = Double.Parse(txtS.Text);
        double nomina = Double.Parse(txtDN.Text);
        double resultado;

        resultado = (sueldo* 0.01125) * 2;

        lblRCV.Text = "$" + Convert.ToString(Math.Round(resultado, 2));
    }
    private void infonavit()
    {
        double sueldo = Double.Parse(txtS.Text);
        double nomina = Double.Parse(txtDN.Text);
        double resultado;

        resultado = sueldoneto * 0.05;

        lblInfo.Text = "$" + Convert.ToString(Math.Round(resultado, 2));
    }

    protected void OnBtnCalClicked(object sender, EventArgs e)
    {
        if (string.IsNullOrEmpty(txtDN.Text) || string.IsNullOrEmpty(txtB.Text) || string.IsNullOrEmpty(txtS.Text))
        {
            Console.Write("Ningún campo debe estar vacío.");
        }
        else
        {
            Aguinaldo();
            infonavit();
            IMSS();
            RCV();
        }

    }

    protected void OnBtnLimClicked(object sender, EventArgs e)
    {
        txtS.Text = "";
        txtB.Text = "";
        txtDN.Text = "";

        lblMostrar.Text = "";
        lblIMSS.Text = "";
        lblRCV.Text = "";
        lblInfo.Text = "";
    }
}
