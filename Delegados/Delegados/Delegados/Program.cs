﻿using System;

public delegate void MiDelegado(int num);//resive solo (int)
public delegate void Delegado(int can);

public class Programa
{
    public static void Main()
    {
        Metodo objMetodo = new Metodo();
        MiDelegado objMiDelegado = new MiDelegado(objMetodo.Metodo1);

        objMiDelegado += objMetodo.Metodo2;
        objMiDelegado(8);

        Delegado objDelegado = new Delegado(objMetodo.Metodo3);

        objDelegado += objMetodo.Metodo3;
        objDelegado(4);
    }
}

public class Metodo
{
    public void Metodo1(int num)  //cumplela condicion del delegado (int)
    {
        Console.WriteLine("num: " + num / 2);
    }

    public void Metodo2(int n)
    {
        Console.WriteLine("n: " + n * 2);
    }

    public void Metodo3(int can)
    {
        Console.WriteLine("can: " + can / 2);
    }

}
