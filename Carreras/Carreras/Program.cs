﻿using System;
using System.Threading;
using System.Threading.Tasks;

namespace CarreraAutos
{
    public class Program
    {
        static void Main(string[] args)
        {
            int y = 5, x = 3, index, i, p;
            var task = new Task[y];
            var randon = new Random();


            for (int z = 0; z < x; z++)
            {
                index = 0;
                p = 0;
                for (i = 0; i < y; i++)
                    task[i] = Task.Run(() => Thread.Sleep(randon.Next(700, 3100)));

                int cancelados = randon.Next(0, 5);


                try
                {
                    index = Task.WaitAny(task);
                    Console.WriteLine("VUELTA {0}\n", z + 1);
                    Console.WriteLine("Auto #{0} ha llegado a la meta.", index + 1);
                    Task.WaitAll(task);
                    for (p = 0; p < y; p++)
                    {
                        Console.WriteLine("El auto #{0}: {1}", p + 1, task[p].Status);
                    }
                }
                catch (AggregateException)
                {
                    Console.WriteLine("Ha ocurrido una excepcion");
                }
            }
        }
    }
}
