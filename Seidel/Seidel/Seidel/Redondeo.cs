﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Redondear
{
    class Redondeo
    {
        private double num;
        private int decimales;

        public double redondeoExacto(double n, int dec)
        {
            //igualo la entrada a mis propieades de clase
            num = n;
            decimales = dec;



            //convierto de double a string el numero que pasaron por parametro para redondear
            string cadena = Convert.ToString(num);
            if (cadena.Contains("."))
            {
                double redondeo = 0;

                //con este string llamado x, sacamos la cantidad de todos los decimales que se generen del numero y creo que funciona para n terminos
                string x = cadena.Substring(cadena.IndexOf(".") + 1, cadena.Length - (cadena.IndexOf(".") + 1));

                if (x.Length > decimales)
                {
                    //codigo valido para el redondeo jaja

                    //le paso el numero a el array de la misma dimensión 
                    char[] numero = cadena.ToCharArray();
                    //declaro un string temporal de ayuda
                    string numeroSinRedondeo = "";
                    //declaro una variable contadora y otra variable que me guarde el indice de donde encuentre el punto
                    int i = 0;
                    int f = 0;



                    bool ban = false;
                    do
                    {

                        if (numero[i] == '.')
                        {

                            f = i;
                            ban = true;
                        }
                        i++;

                    } while (ban != true);
                    //construyo el nuevo numero con los decimales que pidio mas 1
                    for (int s = 0; s < (decimales + 2 + f); s++)
                    {
                        numeroSinRedondeo = numeroSinRedondeo + "" + numero[s];
                    }
                    //saco los decimales del numero en esta variable temporal
                    string c = numeroSinRedondeo.Substring(numeroSinRedondeo.IndexOf(".") + 1, numeroSinRedondeo.Length - (numeroSinRedondeo.IndexOf(".") + 1));
                    //saco el ultimo decimal + 1 con esta variable temporal
                    string d = c.Substring(decimales, 1);

                    //hasta aqui tenemos todo bien

                    //parseo de string a double el ultimo numero
                    double a = double.Parse(d);
                    //con una condicion evaluo el ultimo digito, si es mayor se le saca el valor absoluto, sino se conserva igual
                    if (a >= 5)
                    {
                        a = Math.Abs(a - 10);
                        string cad = "";
                        //construyo mi numero decimal para redondear
                        for (i = 0; i < c.Length - 1; i++)
                        {
                            cad = cad + "0";
                        }
                        cad = cad + "" + a;
                        //hasta este punto tenemos el valor listo para restarlo y sumarlo
                        cad = "0." + cad;

                        //aqui se hacen los redondeos para numeros positivos y negativos
                        if (numero[0].Equals('-'))
                        {
                            redondeo = double.Parse(numeroSinRedondeo) - double.Parse(cad);
                        }
                        else
                        {
                            redondeo = double.Parse(numeroSinRedondeo) + double.Parse(cad);
                        }
                    }
                    else
                    {
                        //si el numero evaluado no es mayor, entonces le quitamos su ultima cifra
                        numeroSinRedondeo = numeroSinRedondeo.Remove(numeroSinRedondeo.Length - 1);

                        redondeo = double.Parse(numeroSinRedondeo);

                    }
                    //se retorna el nuevo numero redondeado
                    return redondeo;
                }
                else
                {
                    return num;
                }

            }
            return num;
        }

    }
}
