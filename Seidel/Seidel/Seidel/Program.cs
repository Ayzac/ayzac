﻿using Redondear;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Seidel
{
    class Program
    {
        static void Main(string[] args)
        {

            Redondeo e = new Redondeo();

            double[] Uno = { -2.5, 3, -3, 10 };
            double[] Dos = { 1, -5, 3, 12 };
            double[] Tres = { -2, 1, -0.1, -5 };
            double[] FUno = { Uno[1] * -1, Uno[2] * -1, Uno[3] };
            double[] FDos = { Dos[0] * -1, Dos[2] * -1, Dos[3] };
            double[] FTres = { Tres[0] * -1, Tres[1] * -1, Tres[3] };
            double[] variables = { 0, 0, 0 };

            int cont = 1;
            double error = 0.0005;
            double errorA = 100;
            double errorB = 100;
            double errorC = 100;

            do
            {

                variables[0] = ((FUno[0] * variables[1]) + (FUno[1] * variables[2]) + (FUno[2])) / Uno[0];

                variables[1] = ((FDos[0] * variables[0]) + (FDos[1] * variables[2]) + (FDos[2])) / Dos[1];

                variables[2] = ((FTres[0] * variables[0]) + (FTres[1] * variables[1]) + (FTres[2])) / Tres[2];

                double a = (Uno[0] * variables[0]) + (Uno[1] * variables[1]) + (Uno[2] * variables[2]);

                double b = (Dos[0] * variables[0]) + (Dos[1] * variables[1]) + (Dos[2] * variables[2]);


                double c = (Tres[0] * variables[0]) + (Tres[1] * variables[1]) + (Tres[2] * variables[2]);

                Console.WriteLine("Iteracion " + cont);
                Console.WriteLine();

                a = e.redondeoExacto(a, 4);
                b = e.redondeoExacto(b, 4);
                c = e.redondeoExacto(c, 4);

                variables[0] = e.redondeoExacto(variables[0], 4);
                variables[1] = e.redondeoExacto(variables[1], 4);
                variables[2] = e.redondeoExacto(variables[2], 4);

                for (int i = 0; i < 3; i++)
                {
                    Console.WriteLine(variables[i]);
                }
                Console.WriteLine();

                errorA = Math.Abs((Uno[3] - a) / Uno[3]) * 100;
                Console.WriteLine(errorA);
                errorB = Math.Abs((Dos[3] - b) / Dos[3]) * 100;
                Console.WriteLine(errorB);
                errorC = Math.Abs((Tres[3] - c) / Tres[3]) * 100;
                Console.WriteLine(errorC);
                cont++;

            } while (error < errorA || error < errorB || error < errorC);
            Console.WriteLine("las raices son:\n" + variables[0] + "x\n" + variables[1] + "y\n" + variables[2] + "z\n");
            Console.ReadLine();
        }
    }
}

