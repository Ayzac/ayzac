import java.util.Scanner;
import java.util.Stack;

public class Pilas {

    static int equalStacks(int[] hal1, int[] hal2, int[] hal3) {
        Stack<Integer> st1 = new Stack<Integer>();
        Stack<Integer> st2 = new Stack<Integer>();
        Stack<Integer> st3 = new Stack<Integer>();

        int st1TotalHeight = 0, st2TotalHeight = 0, st3TotalHeight = 0;
        for (int i = hal1.length - 1; i >= 0; i--) {
            st1TotalHeight += hal1[i];
            st1.push(st1TotalHeight);
        }
        for (int i = hal2.length - 1; i >= 0; i--) {
            st2TotalHeight += hal2[i];
            st2.push(st2TotalHeight);
        }
        for (int i = hal3.length - 1; i >= 0; i--) {
            st3TotalHeight += hal3[i];
            st3.push(st3TotalHeight);
        }

        while (true) {
            if (st1.isEmpty() || st2.isEmpty() || st3.isEmpty())
                return 0;

            st1TotalHeight = st1.peek();
            st2TotalHeight = st2.peek();
            st3TotalHeight = st3.peek();

            if (st1TotalHeight == st2TotalHeight && st2TotalHeight == st3TotalHeight)
                return st1TotalHeight;

            if (st1TotalHeight >= st2TotalHeight && st1TotalHeight >= st3TotalHeight)
                st1.pop();
            else if (st2TotalHeight >= st3TotalHeight && st2TotalHeight >= st3TotalHeight)
                st2.pop();
            else if (st3TotalHeight >= st2TotalHeight && st3TotalHeight >= st1TotalHeight)
                st3.pop();
        }

    }

    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int hal1[] = {6,4,3};
        int hal2[] = {6,4,3};
        int hal3[] = {6,4,3};
        System.out.println(equalStacks(hal1, hal2, hal3));
        in.close();
    }
}