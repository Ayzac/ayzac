﻿using System;
using System.Threading;
using Gtk;

public partial class MainWindow : Gtk.Window
{
    public MainWindow() : base(Gtk.WindowType.Toplevel)
    {
        Build();
    }

    protected void OnDeleteEvent(object sender, DeleteEventArgs a)
    {
        Application.Quit();
        a.RetVal = true;
    }




    protected void OnOnButton2Clicked(object sender, EventArgs e)
    {
        int Pers = Convert.ToInt32(lblPer.Text);
        int Cas = Convert.ToInt32(lblCas.Text);
        int Habi = Convert.ToInt32(lblHabi.Text);
        int Bañ = Convert.ToInt32(lblBañ.Text);
        int Tie = Convert.ToInt32(lblTie.Text);

        Constructora.EmptyCSharpFile cshar = new Constructora.EmptyCSharpFile(Pers, Bañ, Habi, Cas, Tie);

        cshar.Proceso();
        Console.WriteLine(cshar.getmsg());
    }

    protected void OnButton2Clicked(object sender, EventArgs e)
    {
        lblPer.Text = "";
        lblBañ.Text = "";
        lblCas.Text = "";
        lblTie.Text = "";
        lblHabi.Text = "";
    }
}