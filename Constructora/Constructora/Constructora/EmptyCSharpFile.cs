﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Constructora
{
    class EmptyCSharpFile 
    {
        public EmptyCSharpFile(int bla, int pBaño, int pHabita, int pCasa,int ptiempo)
        {
            n = bla;
            Baño = pBaño;
            Habita = pHabita;
            Casa = pCasa;
            tiempo = ptiempo;
        }
        private static Semaphore pooldechalanes;
        private static int _padding;
        private static int n = 0;
        private static int Baño;
        private static int Habita;
        private static int Casa;
        private static int tiempo;
        private static string imprime= " ";

    public void ChalanAlbanil(object EmptyCSharpFile)
        {
            imprime+= "\nChalan de Albañil";
            imprime += "\n Thread {" + Thread.CurrentThread.ManagedThreadId + "}: {" + Thread.CurrentThread.ThreadState + "}, Priority {" + Thread.CurrentThread.Priority + "}";
                    
           imprime+="\nChalan {"+ EmptyCSharpFile+"} esperando turno...";
            pooldechalanes.WaitOne();
            // A padding interval to make the output more orderly.
            int padding = Interlocked.Add(ref _padding, 100);
           imprime+= "\nChalan {"+ EmptyCSharpFile+"} obtiene turno...";
            Thread.Sleep(1000 + padding);
            imprime += "\nChalan {"+ EmptyCSharpFile+"} termina turno...";
            imprime += "\nChalan {" + EmptyCSharpFile + "} libera su lugar {" + pooldechalanes.Release() + "}";
        }


        public async void ConstruirCasas(int cantidad, int Habitaciones, int baño)
        {
            Random rand = new Random();
            Task[] Casas = new Task[cantidad];
            int cont = 0;
            Action<object> action = (object obj) =>
            {
               imprime+=" \nLa casa {"+ Task.CurrentId+"} ya esta lista para construir ";
            };

            for (int i = 0; i < Casas.Length; i++)
            {

                Casas[i] = new Task(action, new string('1', i + 1));
               imprime+="\nComensado la casa {"+Task.CurrentId+"} ";
                if (cont != 0)
                {
                    Habitaciones = rand.Next(1, 10);
                    baño = rand.Next(1, 5);

                }
                    for (int j = 0; j < Habitaciones; j++)
                {
                    imprime+=" \ncreando las habitaciones ";
                }
                for (int h = 0; h < baño; h++)
                {
                    imprime+=" \ncreando los baños";
                }
                Casas[i].Start();
                Thread.Sleep(500);
                cont++;


            }

            imprime+="\nCasas terminadas";

            await Task.WhenAll(Casas);

        }

        public void Proceso()
        {
            pooldechalanes = new Semaphore(0, n);
            for (int i = 1; i <= n; i++)
            {
                Thread t = new Thread(new ParameterizedThreadStart(ChalanAlbanil));
                if (i % 2 == 0)
                {
                    t.Priority = ThreadPriority.AboveNormal;
                }
                else
                {
                    t.Priority = ThreadPriority.BelowNormal;
                }
                t.Start(i);
            }
            ConstruirCasas(Casa, Habita, Baño);
            tiempo = tiempo * 1000;
            Thread.Sleep(tiempo);
            imprime += "\nFin de la obra";
        }
        public string getmsg()
        {
            return imprime;
        }
    }
}

