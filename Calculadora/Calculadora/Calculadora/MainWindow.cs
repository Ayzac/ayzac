﻿using System;
using Gtk;

public partial class MainWindow : Gtk.Window
{
    int cont, opc;
    float num1, num2;

    Calcu Cal = new Calcu();
    float res;

    public MainWindow() : base(Gtk.WindowType.Toplevel)
    {
        Build();
    }

    protected void OnDeleteEvent(object sender, DeleteEventArgs a)
    {
        Application.Quit();
        a.RetVal = true;
    }

    protected void OnB1Clicked(object sender, EventArgs e)
    {
        String display = Pantalla.Text.ToString();
        Pantalla.DeleteText(0, Pantalla.Text.Length);
        Pantalla.InsertText(display + "1");
    }

    protected void OnB2Clicked(object sender, EventArgs e)
    {
        String display = Pantalla.Text.ToString();
        Pantalla.DeleteText(0, Pantalla.Text.Length);
        Pantalla.InsertText(display + "2");
    }

    protected void OnB3Clicked(object sender, EventArgs e)
    {
        String display = Pantalla.Text.ToString();
        Pantalla.DeleteText(0, Pantalla.Text.Length);
        Pantalla.InsertText(display + "3");
    }

    protected void OnB4Clicked(object sender, EventArgs e)
    {
        String display = Pantalla.Text.ToString();
        Pantalla.DeleteText(0, Pantalla.Text.Length);
        Pantalla.InsertText(display + "4");
    }

    protected void OnB5Clicked(object sender, EventArgs e)
    {
        String display = Pantalla.Text.ToString();
        Pantalla.DeleteText(0, Pantalla.Text.Length);
        Pantalla.InsertText(display + "5");
    }

    protected void OnB6Clicked(object sender, EventArgs e)
    {
        String display = Pantalla.Text.ToString();
        Pantalla.DeleteText(0, Pantalla.Text.Length);
        Pantalla.InsertText(display + "6");
    }

    protected void OnB7Clicked(object sender, EventArgs e)
    {
        String display = Pantalla.Text.ToString();
        Pantalla.DeleteText(0, Pantalla.Text.Length);
        Pantalla.InsertText(display + "7");
    }

    protected void OnB8Clicked(object sender, EventArgs e)
    {
        String display = Pantalla.Text.ToString();
        Pantalla.DeleteText(0, Pantalla.Text.Length);
        Pantalla.InsertText(display + "8");
    }

    protected void OnB9Clicked(object sender, EventArgs e)
    {
        String display = Pantalla.Text.ToString();
        Pantalla.DeleteText(0, Pantalla.Text.Length);
        Pantalla.InsertText(display + "9");
    }

    protected void OnBCeroClicked(object sender, EventArgs e)
    {
        String display = Pantalla.Text.ToString();
        Pantalla.DeleteText(0, Pantalla.Text.Length);
        Pantalla.InsertText(display + "0");
    }

    protected void OnBPuntoClicked(object sender, EventArgs e)
    {
        if(cont == 0){
            String display = Pantalla.Text.ToString();
            Pantalla.DeleteText(0, Pantalla.Text.Length);
            Pantalla.InsertText(display + ".");
            cont++;
         }
    }

    protected void OnBVaciarClicked(object sender, EventArgs e)
    {
        Pantalla.DeleteText(0, Pantalla.Text.Length);
        cont = 0;
    }

    protected void OnBDividirClicked(object sender, EventArgs e)
    {
        if (opc == 0)
        {
            num1 = Convert.ToSingle(Pantalla.Text);
            Pantalla.DeleteText(0, Pantalla.Text.Length);
            cont = 0;
            opc = 1;
        }
    
    }

    protected void OnBXClicked(object sender, EventArgs e)
    {
        if (opc == 0)
        {
            num1 = Convert.ToSingle(Pantalla.Text);
            Pantalla.DeleteText(0, Pantalla.Text.Length);
            cont = 0;
            opc = 2;
        }
    }

    protected void OnButton16Clicked(object sender, EventArgs e)
    {
        if (opc == 0)
        {
            num1 = Convert.ToSingle(Pantalla.Text);
            Pantalla.DeleteText(0, Pantalla.Text.Length);
            cont = 0;
            opc = 3;
        }
    }

    protected void OnButton20Clicked(object sender, EventArgs e)
    {
        if (opc == 0)
        {
            num1 = Convert.ToSingle(Pantalla.Text);
            Pantalla.DeleteText(0, Pantalla.Text.Length);
            cont = 0;
            opc = 4;
        }
    }

    protected void OnButton19Clicked(object sender, EventArgs e)
    {
        if (opc !=0){
            num2 = Convert.ToSingle(Pantalla.Text);
            Pantalla.DeleteText(0, Pantalla.Text.Length);
            switch (opc)
            {
                case 1:
                    res = Cal.divicion(num1, num2);
                    Pantalla.InsertText(Convert.ToString(res));
                    break;
                case 2:
                    res = Cal.multiplicacion(num1, num2);
                    Pantalla.InsertText(Convert.ToString(res));
                    break;
                case 3:
                    res = Cal.resta(num1, num2);
                    Pantalla.InsertText(Convert.ToString(res));
                    break;
                case 4:
                    res = Cal.suma(num1, num2);
                    Pantalla.InsertText(Convert.ToString(res));
                    break;
            }
            opc = 0;
        }
    }
}
