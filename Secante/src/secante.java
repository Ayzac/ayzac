import java.util.Scanner;

public class secante {


    static double f(double x)
    {
        return (((0.5)*(Math.pow(x, 3))) + ((3.82)*(x)) - (27));
    }

    public static void main(String args[]) {

        Scanner teclado = new Scanner(System.in);

        double a, b, e = 0, eR;
        double xR, xRN = 0;
        System.out.println("Ingresa el intervalo de a: ");
        a = teclado.nextDouble();
        System.out.println("Ingresa el intervalodes de b: ");
        b = teclado.nextDouble();
        System.out.println("Ingresa el porcentaje de error: ");
        e = teclado.nextDouble();
        xR = a - ((f(a)*(b-a))/(f(b)-f(a)));

        System.out.println();

        do {
            if(f(a)*f(xR)==0)
                System.out.printf("La raiz es: %f", b);
            else if(f(a)*f(xR)<0)
                b=xR;
            else
                a=xR;

            xRN = a - ((f(a)*(b-a))/(f(b)-f(a)));
            eR = Math.abs((xRN - xR) / (xRN)) * 100;
            System.out.printf("\nXR: %f   Aproximacion: %f     Error Relativo: %f", xR, xRN, eR);
            xR=xRN;
        }while(eR >= e);
    }
}