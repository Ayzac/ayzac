﻿using System;
using Gtk;
using System.IO;




public partial class MainWindow : Gtk.Window
{
    int Bandera = 0;
    string datoEntrada;
    string datoSalida;
    string Empleado;

    public string path = @"/home/isaac/Escritorio/Datos.txt";

    public MainWindow() : base(Gtk.WindowType.Toplevel)
    {
        Build();
    }

    protected void OnDeleteEvent(object sender, DeleteEventArgs a)
    {
        Application.Quit();
        a.RetVal = true;
    }

    protected void OnButton1Clicked(object sender, EventArgs e)
    {
        string readText = File.ReadAllText(path);
        Console.WriteLine(readText);

    }

    protected void OnEntrarClicked(object sender, EventArgs e)
    {
        Empleado = comboboxentry2.ActiveText;
        if (!File.Exists(path))
        {
            using (StreamWriter sw = File.CreateText(path))
            {
                sw.WriteLine(Empleado);
                sw.WriteLine(datoEntrada);

            }
        }
        else
        {
            string append = "";
            if (Bandera == 1)
            {
                append = "Entrada\n" + Empleado + "\n" + datoEntrada + Environment.NewLine;
            }
            else if (Bandera == 2)
            {
                append = "Salida\n" + Empleado + "\n" + datoSalida + Environment.NewLine;
            }

            File.AppendAllText(path, append);

        }

}

    protected void OnRadio2Clicked(object sender, EventArgs e)
    {
        Bandera = 2;
    }

    protected void OnRadio1Clicked(object sender, EventArgs e)
    {
        Bandera = 1;
    }

    protected void OnComboboxentry1Changed(object sender, EventArgs e)
    {
        datoEntrada = Fecha.ActiveText;
        datoSalida = Fecha.ActiveText;
    }
}