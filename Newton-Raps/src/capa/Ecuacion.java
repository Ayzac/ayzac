package capa;

public class Ecuacion
{
    public double[] Funcion;
    public double[] Potencia;
    public double[] Producto;
    public double[] FuncionPrima;
    public double[] PotenciaPrima;
    public double[] ProductoPrima;

    String uno = "", dos = "", tres = "";
    public String resultado;



    public void Iniciar(int a){

        Funcion = new double[a];
        Potencia = new double[a];
        Producto = new double[a];

    }

    public String mostrarFuncion(int valor)
    {

        if(valor==1)
        {



            for (int i = 0; i < Funcion.length; i++) {
                if (Funcion[i] == 100) {
                    if (Producto[i] > 1 || Producto[i] == 1) {
                        if (i == 0 || i == Funcion.length) {
                            if (Producto[i] == 1) {
                                uno = "X";
                            } else {
                                uno = Producto[i] + "X";
                            }
                        } else {
                            if (Producto[i] == 1) {
                                uno = "+X";
                            } else {
                                uno = "+" + Producto[i] + "X";
                            }
                        }
                    } else if (Producto[i] < 0) {
                        if (Producto[i] == -1) {
                            uno = "-X";
                        } else {
                            uno = Producto[i] + "X";
                        }
                    } else if (Producto[i] < 1 && Producto[i] > 0) {
                        if (i == 0) {
                            uno = Producto[i] + "X";
                        } else {
                            uno = "+" + Producto[i] + "X";
                        }
                    }

                    if (Potencia[i] < 2) {
                        dos = uno;
                    } else {
                        dos = uno + "^" + Potencia[i];
                    }

                } else {
                    if (Producto[i] > 1 || Producto[i] == 1) {
                        if (i == 0 || i == Funcion.length) {
                            uno = (Producto[i] * Funcion[i]) + "";
                        } else {
                            uno = "+" + (Producto[i] * Funcion[i]);
                        }


                    } else if (Producto[i] < 0) {
                        uno = (Producto[i] * Funcion[i]) + "";
                    }


                    dos = uno;
                }


                tres = tres + dos;


            }

            return tres;
        }
        else
        {



            for (int i = 0; i < FuncionPrima.length; i++) {
                if (FuncionPrima[i] == 100) {
                    if (ProductoPrima[i] > 1 || ProductoPrima[i] == 1) {
                        if (i == 0 || i == FuncionPrima.length) {
                            if (ProductoPrima[i] == 1) {
                                uno = "X";
                            } else {
                                uno = ProductoPrima[i] + "X";
                            }
                        } else {
                            if (ProductoPrima[i] == 1) {
                                uno = "+X";
                            } else {
                                uno = "+" + ProductoPrima[i] + "X";
                            }
                        }
                    } else if (ProductoPrima[i] < 0) {
                        if (ProductoPrima[i] == -1) {
                            uno = "-X";
                        } else {
                            uno = ProductoPrima[i] + "X";
                        }
                    } else if (ProductoPrima[i] < 1 && ProductoPrima[i] > 0) {
                        if (i == 0) {
                            uno = ProductoPrima[i] + "X";
                        } else {
                            uno = "+" + ProductoPrima[i] + "X";
                        }
                    }

                    if (PotenciaPrima[i] < 2) {
                        dos = uno;
                    } else {
                        dos = uno + "^" + PotenciaPrima[i];
                    }

                } else {
                    if (ProductoPrima[i] > 1 || ProductoPrima[i] == 1) {
                        if (i == 0 || i == FuncionPrima.length) {
                            uno = (ProductoPrima[i] * FuncionPrima[i]) + "";
                        } else {
                            uno = "+" + (ProductoPrima[i] * FuncionPrima[i]);
                        }


                    } else if (ProductoPrima[i] < 0) {
                        uno = (ProductoPrima[i] * FuncionPrima[i]) + "";
                    }


                    dos = uno;
                }


                tres = tres + dos;


            }

            return tres;
        }


    }


    public int contar()
    {
        int c=0;
        for(int i = 0 ; i < Funcion.length ; i++)
        {
            if(Funcion[i]==100)
            {
                c++;
            }
        }
        return c;
    }

    public void derivada()
    {
        int a = contar();

        FuncionPrima = new double[a];
        ProductoPrima = new double[a];
        PotenciaPrima = new double[a];

        int s = 0 ;
        for(int i = 0 ; i < Funcion.length; i++)
        {
            if(Funcion[i] == 100)
            {
                ProductoPrima[s] = Potencia[i] * Producto[i] ;
                if(Potencia[i] ==1)
                {
                    FuncionPrima[s] = 1;
                    PotenciaPrima[s] = 1;
                }
                else
                {
                    PotenciaPrima[s] = Potencia[i] - 1;
                    FuncionPrima[s]  = 100;
                }
                s++;
            }
        }

        tres = "";
        uno = "";
        dos = "";

    }


}