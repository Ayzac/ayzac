package capa;
import java.util.Scanner;

public class Principal
{
    double auxIteracion = 100;
    double aux2Iteracion = 0;
    double aux2IteracionPrima =0;
    double auxIteracionPrima = 100;
    int Decimales;

    boolean[] validar;
    boolean[] validarPrima;

    int Contador = 0;
    int ContadorPrima = 0;

    public static void main(String[] args)
    {

        Scanner c = new Scanner(System.in);
        int Decimal = 0;
        Ecuacion e = new Ecuacion();

        System.out.println("ingrese los elementos que tendra la ecuacion: ");
        e.Iniciar(c.nextInt());

        for (int i = 0; i < e.Funcion.length; i++) {

            System.out.println("Constante o variable?\nEscribe C o V");

            String primeraFase = c.next();
            if (primeraFase.equals("C") || primeraFase.equals("c")) {
                System.out.println ("La constante es positiva o negativa?\nEscribe P o N");

                String constanteFaseUno = c. next();

                boolean flagUno = true;
                boolean operacion = false;
                do {
                    if (constanteFaseUno.equals("P") || constanteFaseUno.equals("p")) {
                        operacion = true;
                    } else if (constanteFaseUno.equals("N") || constanteFaseUno.equals("n")) {
                        operacion = false;
                    } else {
                        System.out.println("No puedes colocar un digito o caracter distinto");

                        constanteFaseUno = "";
                        flagUno = false;
                    }
                } while (flagUno != true);


                System.out.println ("Ingrese el numero, solo valores positivos:");

                String constanteFaseDos = c.next();


                e.Funcion[i] = Double.parseDouble(constanteFaseDos);

                if (operacion) {
                    e.Producto[i] = 1;
                } else {
                    String aux2 = "-";

                    String aux = aux2 + 1;


                    e.Producto[i] = Double.parseDouble(aux);
                }
                e.Potencia[i] = 1;

            } else if (primeraFase.equals("V") || primeraFase.equals("v")) {


                System.out.println("Tiene constante para multiplicar por la variable?");

                double variableFaseDos = c.nextDouble();

                System.out.println("La Variable es positiva o negativa?\nEscribe P o N");


                String variableFaseUno = c.next();
                boolean flagUno = true;
                boolean operacion = false;

                do {
                    if (variableFaseUno.equals("P") || variableFaseUno.equals("p")) {
                        operacion = true;
                    } else if (variableFaseUno.equals("N") || variableFaseUno.equals("n")) {
                        operacion = false;
                    } else {
                        System.out.println("No puedes colocar un digito o caracter distinto");

                        variableFaseUno = "";
                        flagUno = false;
                    }
                } while (flagUno != true);
                System.out.println("Ingrese el exponente de la variable:");
                double variableFaseTres = c.nextDouble();
                e.Potencia[i] = variableFaseTres;


                if (operacion) {
                    e.Producto[i] = variableFaseDos;
                } else {
                    String aux2 = "-";

                    String aux = aux2 + variableFaseDos;

                    variableFaseDos = Double.parseDouble(aux);

                    e.Producto[i] = variableFaseDos;
                }

                e.Funcion[i] = 100;

            } else {
                System.out.println("No puedes colocar un digito o caracter distinto");
                primeraFase = "";
                i--;
            }
        }


        //hasta este punto ya tenemos la funcion
        //lo siguiente es mostrarla en pantalla

        System.out.println("esta ha sido la funcion");
        System.out.println("" + e.mostrarFuncion(1));
        e.derivada();
        System.out.println("este es la derivada");
        System.out.println("" + e.mostrarFuncion(2));


        Principal p = new Principal();

        p.setBool(e.Funcion.length);

        //numero de decimales a trabajar
        System.out.println("Dame el numero de decimales a trabajar: ");

        String decimales = c.next();
        try {
            Decimal = Integer.parseInt(decimales);
            p.setDecimal(Decimal);
        } catch (Exception w) {

        }

        //lo siguiente es pedir el intervalo de valores donde se analizara la funcion.

        System.out.println("Ingrese el intervalo: ");

        String intervalo = c.next();

        double ResIntervalo = p.Iteracion(Double.parseDouble(intervalo), e);
        System.out.println(ResIntervalo);
        System.out.println();

        double eme = p.IteracionDerivada(Double.parseDouble(intervalo),e);


        System.out.println(eme);
        //pedimos porcentaje de error
        System.out.println("Dame el portentaje de error: ");

        String portError = c.next();

        double Error = 0;
        try {
            Error = Double.parseDouble(portError);
        } catch (Exception w) {

        }

        //Aqui hacemos la integracion del metodo de Netwon

        double xr = Double.parseDouble(intervalo);
        double xrR;
        double error = 0;
        double item = 1;

        do
        {

            xrR = xr - (p.Iteracion(xr,e)/p.IteracionDerivada(xr,e));
            xrR = p.setRedondeo(xrR, Decimal);


            error =Math.abs(((xrR - xr) / xrR))*100;
            System.out.println("es el error absoluto "+error +"%");
            xr = xrR;

            System.out.println("este es el valor de x " + xrR);
            item++;

        }while(error<Error || item<50);

        System.out.println("es el valor final "+ xr);


    }
    //fuera del main

    public double Iteracion(double a, Ecuacion e) {
        for (int i = 0; i < e.Funcion.length; i++) {
            if (Contador == 0) {
                if (e.Funcion[i] == auxIteracion) {
                    e.Funcion[i] = a;
                    validar[i] = true;
                }
            } else {
                if (e.Funcion[i] == auxIteracion && validar[i] == true) {
                    e.Funcion[i] = a;
                }
            }
        }
        auxIteracion = a;
        double[] funcionFalsa = new double[e.Funcion.length];
        for (int i = 0; i < e.Funcion.length; i++) {
            aux2Iteracion = Math.pow(e.Funcion[i], e.Potencia[i]);
            aux2Iteracion = aux2Iteracion * e.Producto[i];
            funcionFalsa[i] = aux2Iteracion;
        }
        double aux4 = 0;
        double aux5 = 0;
        for (int i = 0; i < e.Funcion.length; i++) {
            aux4 = funcionFalsa[i];
            aux5 = aux5 + aux4;
        }
        Contador++;
        return setRedondeo(aux5, Decimales);
    }


    public double IteracionDerivada(double a, Ecuacion e) {
        for (int i = 0; i < e.FuncionPrima.length; i++) {
            if (ContadorPrima == 0) {
                if (e.FuncionPrima[i] == auxIteracionPrima) {
                    e.FuncionPrima[i] = a;
                    validarPrima[i] = true;
                }
            } else {
                if (e.FuncionPrima[i] == auxIteracionPrima && validarPrima[i] == true) {
                    e.FuncionPrima[i] = a;
                }
            }
        }
        auxIteracionPrima = a;
        double[] funcionFalsaPrima = new double[e.FuncionPrima.length];
        for (int i = 0; i < e.FuncionPrima.length; i++) {
            aux2IteracionPrima = Math.pow(e.FuncionPrima[i], e.PotenciaPrima[i]);
            aux2IteracionPrima = aux2IteracionPrima * e.ProductoPrima[i];
            funcionFalsaPrima[i] = aux2IteracionPrima;
        }
        double aux4 = 0;
        double aux5 = 0;
        for (int i = 0; i < e.FuncionPrima.length; i++) {
            aux4 = funcionFalsaPrima[i];
            aux5 = aux5 + aux4;
        }
        ContadorPrima++;
        return setRedondeo(aux5, Decimales);
    }



    //funcion para redondear resultados
    public double setRedondeo(double a, int b) {
        Redondeo c = new Redondeo();
        return c.redondear(a, b);
    }


    public void setDecimal(int a) {
        this.Decimales = a;
    }


    public void setBool(int a) {
        validar = new boolean[a];
        validarPrima = new boolean[a];
    }

    public int getDecimal() {
        return Decimales;
    }
}