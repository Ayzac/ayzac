﻿using System;
using System.Data.SqlClient;

namespace AdoNetConsoleApplication
{
    class AdoNetConsoleApplication
    {
        static void Main(string[] args)
        {
            new AdoNetConsoleApplication().CreateTable();//crear objeto anomimo y mandar llamar el metododo
        }

        public void CreateTable()
        {
            SqlConnection con = null;
            try
            {
                con = new SqlConnection("data source =.; database = Student; integrated security = SSPI");
                // string sql = "create table student(id int not null, name varchar(100), email varchar(50), join_date date)";
                //string sql = "insert into student    (id, name, email, join_date)values('101', 'Ronald Trump', 'ronald@example.com', '1/12/2017')";
                // string sql = "select * from student";
                string sql = "delete from student where id='102'";
                SqlCommand cm = new SqlCommand(sql,con);//modifcacion vista en stack overflow porque me daba errrores
                con.Open();//abrir la conexion
                 cm.ExecuteNonQuery();//ejecutar la consulta a la BD
                Console.WriteLine("Record Deleted Successfully");
                // Console.WriteLine("Table created Successfully");

                /* SqlDataReader sdr = cm.ExecuteReader();//ejecuta consulta de leer
                 while (sdr.Read())
                 {
                     Console.WriteLine(sdr["id"] + " " + sdr["name"] + " " + sdr["email"]+""+sdr["join_date"]); // Displaying Record
                 }
                 //Console.WriteLine("Record Inserted Successfully");*/
            }
            catch (Exception e)
            {

                Console.WriteLine("OOPs, something went wrong." + e);
            }
            finally
            {
                con.Close();//finally se ejecuta siempre sin importar si se lanzo o no una excepcion, en este caso cierra conexion con la BD
            }
        }
    }
}
